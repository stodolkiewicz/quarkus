package tech.donau.coourse;

import tech.donau.coourse.service.GreetingService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {
    @Inject
    GreetingService greetingService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@QueryParam("name") String name) {
        return greetingService.sayHello(name);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String addHello(@HeaderParam("token") String hName, @QueryParam("name") String aName) {
        String token = hName != null ? hName : aName;
        if(!token.equalsIgnoreCase("123")) {
            throw new RuntimeException("Oh no!");
        }

        return "{ \"key\": \"" + aName + "\" }";
    }
}
